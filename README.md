# needle.tex

Needleman-Wunsch Alignment Algorithm in LuaLaTeX.

<img src="media/aligntex_v1.png" width=250 alt="An example of alignment matrix and path built with needle.tex">

